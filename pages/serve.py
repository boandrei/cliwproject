#!python2
# -*- coding: utf-8 -*-
import codecs
import datetime
import mimetypes
import sys
from http.server import BaseHTTPRequestHandler, HTTPServer
import os
import re
try:
    from jinja2 import Template, Environment, FileSystemLoader, nodes
    from jinja2.ext import Extension
except ImportError:
    print("Jinja template engine is not installed. Fix this with `pip install jinja2`")
    exit(1)

DEBUG = True
TEMPLATE_DEBUG = False

jinja_env = Environment(extensions=[])


def do_indent_tabs(s, width=1, indentfirst=False, usetabs=False):
    """
    Yoinked from Jinja2 source. thx babes.
    """
    tab = u'\t'
    if not usetabs:
        tab = ' '
    indention = tab * width
    rv = (u'\n' + indention).join(s.splitlines())
    if indentfirst:
        rv = indention + rv
    return rv


jinja_globals = {}
jinja_env.loader = FileSystemLoader('.')
jinja_env.filters['indent'] = do_indent_tabs

server_port = 8000

# Could come in handy.
jinja_globals["current_year"] = datetime.datetime.now().year
jinja_globals["current_datestamp"] = datetime.datetime.now()

jinja_env.globals.update(jinja_globals)


class JinjaHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        try:
            path = self.path
            if path.endswith('/'):
                path += 'index.html'
            
            print(path)
            if re.match('^\/[A-Za-z0-9]+$', path):
                path += '.html'
            print(path)

            path = path.strip('/')
            self.send_response(200)
            content_type = mimetypes.guess_type(path)[0]
            if path.endswith('.js'):
                content_type = 'application/javascript'
            self.send_header('Content-type', content_type)
            self.end_headers()


            jinja_env.globals.update({'path': path})
            if 'public' in path:
                path = os.path.join('..', path)

            data = None
            with open(path, 'rb') as f:
                if not 'public' in path:
                    data = jinja_env.from_string(f.read().decode('utf8')).render().encode('utf-8')
                else:
                    data = f.read()
            self.wfile.write(data)
            self.wfile.close()
            return
        except IOError as e:
            import traceback
            top = traceback.extract_stack()[-1]
            self.wfile.write(
                '<code>Error: %s<br>Details: %s</code>' %
                (
                    type(e).__name__,
                    # os.path.basename(top[0]),
                    # str(top[1]),
                    e
                )
            )

            return
            # self.send_error(404,'File Not Found: %s' % self.path)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        try:
            sys.argv[1] = int(sys.argv[1])
            server_port = sys.argv[1]
        except:
            print('Invalid port number:', sys.argv[1])
            exit(1)

    try:
        httpd = HTTPServer(('', server_port), JinjaHandler)
        print('Server started on port %d, press Ctrl-C to stop' % server_port)
        httpd.serve_forever()
    except KeyboardInterrupt:
        print('\nShutting down...')
