function addChapters(room, outline, main_node, numbering, owner = false) {
    let id = numbering.replace(/\./g, '_');
    let node = createNode(
        `<li class="${ owner ? 'change_chapter' : ''}">
            <span class="numbering" id="numbering_${id}">${numbering}</span><!--
            --><span class="chapter_name">${outline.content}</span>
        </li>`
    );

    if (owner) {
        node.addEventListener('click', function(e) {
            let numbering = this.querySelector('[id^=numbering_]');

            let nodeNumber = numbering.innerHTML;
            db.collection('rooms')
                .doc(room.id)
                .update({'current_chapter': nodeNumber})
                .then(r => {
                    addMessage('Chapter changed', 'success');
                })
                .catch(r => {
                    addMessage(r, 'error');
                });
        });
    }
    main_node.appendChild(node);
    if ('subchapters' in outline && outline.subchapters.length > 0) {
        for (let [index, tmp] of outline.subchapters.entries()) {
            addChapters(room, tmp, main_node, `${numbering}.${index + 1}`, owner);
        }
    }
}

function setCurrentChapter(current_chapter) {
    let id = current_chapter.replace(/\./g, '_');
    for (let node of document.querySelectorAll('.current_chapter')) {
        node.classList.remove('current_chapter');
    }
    let node = document.querySelector(`#numbering_${id}`).parentNode;
    if (!node.classList.contains('current_chapter')) {
        node.classList.add('current_chapter');
    }
}

function listenForChanges(room, user) {
    db.collection('rooms')
        .doc(room.id)
        .onSnapshot(snap => {
            setCurrentChapter(snap.data().current_chapter);
        })
}


export function setupOutline(room, user) {
    let owner = false;
    if (room.uid === user.uid) {
        owner = true;
    }

    let title = createNode(`
        <h2>Outline</h2>
    `);
    let list = createNode(`
        <ul id="outline"></ul>
    `);

    let container =  document.getElementById('room_outline');
    container.appendChild(title);
    container.appendChild(list);

    for (let [index, chapter] of room.outline.entries()) {
        addChapters(room, chapter, list, `${index + 1}`, owner);
    }
    setCurrentChapter(room.current_chapter);
    listenForChanges(room, user);
    //let access_key = room.access_key;
}
