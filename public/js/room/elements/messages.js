import {addMessageComponent} from "./message_container.js";

function sendMessage(room) {
    firebase.auth().onAuthStateChanged(user => {
        let message = {};
        let messageInput = document.getElementById('leave_message');
        message.message = messageInput.value;
        messageInput.value = '';
        let chapterNode = document.querySelector('.current_chapter span.chapter_name');
        message.chapter = chapterNode ? chapterNode.innerHTML.trim() : 'No chapter';
        message.user = user.email ? user.email : 'Anonymous user';
        message.userID = user.uid;
        message.timestamp = + new Date();
        message.type = (room.uid === user.uid) ? 'presenter' : 'other';

        db.collection('rooms')
            .doc(room.id)
            .collection('messages')
            .add(message)
            .then(r => {
                addMessage('Your message has been submitted', 'success');
            })
            .catch(r => {
                addMessage('There has been an error while submitting the message', 'error');
            })
    });
    return false;
}


function setupMessages(room, user) {
    if (room.feedback_type && room.feedback_type.includes('form_message')) {
        let messagesContainer = createNode(`
            <div class="messages_wrapper">
                <h2>Messages</h2>
                <div class="messages" id="messages"></div>
                <form id="leave_message_form">
                    <input id="leave_message" type="text" placeholder="Leave a message">
                </form>
            </div>
        `);

        messagesContainer.querySelector('#leave_message_form').onsubmit = function(e) {
            event.preventDefault(); return sendMessage(room);
        };

        let feedbackContainer = document.getElementById('feedback');
        feedbackContainer.appendChild(messagesContainer);

        db.collection('rooms')
            .doc(room.id)
            .collection('messages')
            .onSnapshot(messagesSnap => {
                messagesSnap.docChanges().sort((a, b) => a.doc.data().timestamp - b.doc.data().timestamp);
                messagesSnap.docChanges().forEach(change => {
                    // Votes for this particular message
                    let votedRef = db.collection('rooms')
                        .doc(room.id)
                        .collection('messages')
                        .doc(change.doc.id)
                        .collection('voted');

                    if (change.type === 'added') {
                        votedRef
                            .get()
                            .then(snap => {
                                if (change.doc.data().type === 'presenter') {
                                    addMessageComponent(room, user, change.doc);
                                }
                                else {
                                    if (room.uid === user.uid) {
                                        if (snap.size >= room.room_votes) {
                                            addMessageComponent(room, user, change.doc);
                                        }
                                        else {
                                            // Listen for when this message gets enough votes
                                            let unsubscribe = votedRef.onSnapshot(snap => {
                                                if (snap.size >= room.room_votes) {
                                                    addMessageComponent(room, user, change.doc);
                                                    unsubscribe();
                                                }
                                            })
                                        }
                                    }
                                    else {
                                        addMessageComponent(room, user, change.doc);
                                    }
                                }
                            });
                    }
                });
            });
    }
}

export function setupFeedback(room, user) {
    setupMessages(room, user);
}