import {setupQuizzes} from './quizzes.js';

export function setupButtons(room, user) {
    if (room.uid === user.uid) {
        let manage_room = createNode(`
            <button class="room_transition_button" data-active="false" id="manage_room">
                Manage Room
            </button>
        `);

        let statistics = createNode(`
            <button class="room_transition_button" data-active="false" id="statistics">
                Statistics
            </button>
        `);

        document.getElementById('room_buttons').appendChild(manage_room);

    }
    let leave_room = createNode(`
        <button class="room_transition_button">
            Leave room
        </button>
    `);
    leave_room.addEventListener('click', e => {
        setCookie('access_key', '');
        location.replace('/');
    });
    document.getElementById('room_buttons').appendChild(leave_room);


    db.collection('rooms')
        .doc(room.id)
        .collection('quizzes')
        .onSnapshot(snap => {
            db.collection('rooms')
                .doc(room.id)
                .collection('quizzes')
                .where('status', '==', 'active')
                .get()
                .then(snap => {
                    if (snap.size > 0 && room.uid !== user.uid) {
                        let take_quiz = createNode(`
                            <button class="room_transition_button" data-active="false" id="quiz_transition">
                                Take the quiz
                            </button>
                        `);
                        take_quiz.addEventListener('click', e => {

                            if (e.target.dataset.active === 'true') {
                                e.target.innerText = 'Take the quiz';
                                changeSlide('feedback');
                            }
                            else {
                                changeSlide('quiz');
                                e.target.innerText  = 'Back to Room';
                            }
                            e.target.dataset.active = (e.target.dataset.active === 'true' ? 'false' : 'true');
                        });
                        document.getElementById('room_buttons').appendChild(take_quiz);
                        setupQuizzes(room, user);
                    }
                    else {
                        let quizButton = document.querySelector('#quiz_transition');
                        if (quizButton) {
                            quizButton.remove();
                        }
                    }
                })
        });

    if (document.getElementById('manage_room')) {
        document.getElementById('manage_room').addEventListener('click', e => {
            if (e.target.dataset.active === 'true') {
                e.target.innerText = 'Manage Room';
                changeSlide('feedback');
            }
            else {
                e.target.innerText  = 'Back to Room';
                changeSlide('manage');
            }

            document.getElementById('statistics').dataset.active = 'false';
            document.getElementById('statistics').innerText = 'Statistics';
            e.target.dataset.active = (e.target.dataset.active === 'true' ? 'false' : 'true');
        })
    }

    if (document.getElementById('statistics')) {
        document.getElementById('statistics').addEventListener('click', e => {
            if (e.target.dataset.active === 'true') {
                e.target.innerText = 'Statistics';
                changeSlide('feedback');
            }
            else {
                e.target.innerText = 'Back to Room';
                changeSlide('statistics');
            }

            document.getElementById('manage_room').innerText = 'Manage Room';
            document.getElementById('manage_room').dataset.active = 'false';
            e.target.dataset.active = (e.target.dataset.active === 'true' ? 'false' : 'true');
        })
    }

}