function createQuiz(room, user, quiz) {
    let quizWrapper = document.getElementById('quiz_wrapper');

    let data = {
        'access_key': getCookie('access_key')
    };

    let quizNode = createNode(`
        <div data-quiz-id="${quiz.id}" class="quiz_outer_wrapper">
            <div class="quiz_inner_wrapper slide">
                <div class="quiz_description">${quiz.name}</div>
                    <div class="triggers">
                        <div class="start_quiz started"><button>Stop quiz</button></div>
                        <div class="start_quiz"><button>Start quiz</button></div>
                        <div class="show_results"><button>Show results</button></div>
                        <div class="quiz_settings"><button>Quiz settings</button></div>
                    </div>
                </div>
                <div class="quiz_info">
                    <div class="show_results_info">
                        <div data-slide-id="2" class="quiz slide" id="quiz">
                            <div class="quiz_question_wrapper"></div>
                         </div>
                    </div>
                    <div class="quiz_settings_info">
                        <form class="custom_form quiz_settings_form">
                            <ul>
                                <li>
                                    <input 
                                        type="text" 
                                        name="duration" 
                                        class="field-style" 
                                        placeholder="Quiz time" 
                                        value="${quiz.duration}"
                                    />
                                </li>
                                <li>
                                    <input type="submit" value="Save modifications"/>
                                </li>
                            </ul>
                         </form>
                    </div>
                 </div>
            </div>
        </div>
    `);
    /*
    quiz.questions.forEach(question => {
        quizContent += '<h2 class="quiz_question"> '+ question.content +' </h2>';
        quizContent += '<ul data-quiz-question="'+ question.id +'">';

        question.answers.forEach(answer => {

            let mostAnswered = answer.most_answered ? 'most_answered' : '';

            quizContent += '<li class="quiz_answer '+ mostAnswered +'" data-quiz-answer="'+ answer.id +'">'+ answer.content +' ( '+ answer.response_percent +' % ) </li>';
        });

        quizContent += '</ul>';
    });
    */

    quizSettingsControl()

}