export function addMessageComponent(room, user, messageRef) {
    let messageData = messageRef.data();
    let messageNode = createNode(`
        <div data-message-id="${messageRef.id}" class="message ${messageData.type === 'presenter' ? 'presenter_message':''}">
            <div class="message_meta">
                <div class="message_chapter">${messageData.chapter}</div>
                ${messageData.type !== 'presenter' ? '<div class="message_votes"><span class="votes_number"></span> votes</div>' : ''}
                <div data-user-id="${messageData.userID}" class="message_user">${messageData.user}</div>
            </div>
            <div class="message_content">${messageData.message}</div>
        </div>
    `);

    if (messageData.type !== 'presenter') {
        let votedRef = db.collection('rooms')
            .doc(room.id)
            .collection('messages')
            .doc(messageRef.id)
            .collection('voted');


            votedRef.onSnapshot(snap => {
                snap.docChanges().forEach(change => {
                    if (change.type === 'added') {
                        if (change.doc.id === user.uid) {
                            messageNode.querySelector('.message_votes').classList.add('voted');
                        }
                    }

                });
                let votesNode = messageNode.querySelector(`.votes_number`);
                votesNode.innerHTML = `${snap.size}`;
            });


        messageNode.querySelector('.message_votes').addEventListener('click', function(e) {
            votedRef
                .doc(user.uid)
                .get()
                .then(user_voted => {
                    if (user_voted.exists) {
                        addMessage('You already voted for this message', 'error');
                    }
                    else {
                        votedRef
                            .doc(user.uid)
                            .set({});
                    }
                });
        });
    }
    document.getElementById('messages').appendChild(messageNode);
}