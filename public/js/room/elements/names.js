export function setupNames(room, user) {
    if (room.room_name && room.room_topic) {
        let room_name = createNode(`
            <h2 id="room_name">${room.room_name}</h2>
        `);
        let room_topic = createNode(`
            <h4 id="room_topic">${room.room_topic}</h4>
        `);

        let room_key = createNode(`
            <h4 id="room_topic">Key: ${room.access_key}</h4>
        `);

        let room_info = document.getElementById('room_info');
        room_info.appendChild(room_name);
        room_info.appendChild(room_topic);
        room_info.appendChild(room_key);
    }
}