function createQuiz(room, user, quiz) {

    let parent = document.getElementById('quiz_wrapper');
    parent.innerHTML = '';

    let title = createNode(`
        <h2 class="room_content_title">Quiz: ${quiz.name}</h2>
    `);
    let duration = createNode(`
        <div class="duration"><span id="time_left">${quiz.duration} minutes</span></div>
    `);
    /*
    setInterval(() => {
        document.getElementById('time_left').innerText = get_time_diff(
            quiz.expiry_time.date
        );
    }, 1000);
    */
    let innerQuiz = createNode(`
        <div id="inner_quiz"></div>
    `);

    parent.appendChild(title);
    parent.appendChild(duration);
    parent.appendChild(innerQuiz);


    innerQuiz.dataset.quizId = quiz.id;
    let questions_collection = db.collection('rooms')
        .doc(room.id)
        .collection('quizzes')
        .doc(quiz.id)
        .collection('questions');

    questions_collection
        .orderBy('index')
        .get()
        .then(questions => {
            questions.forEach(question => {
                let data = question.data();
                let q_node = createNode(`
                    <div class="quiz_question_wrapper">
                        <h2 class="quiz_question">${data.content}</h2>
                        <ul data-quiz-question="${question.id}"></ul>
                    </div>
                `);

                questions_collection
                    .doc(question.id)
                    .collection('answers')
                    .orderBy('index')
                    .get()
                    .then(answers => {
                        answers.forEach(answer => {
                            let answer_data = answer.data();
                            let answer_node = createNode(`
                                <li class="quiz_answer" data-quiz-answer="${answer.id}">${answer_data.content}</li>
                            `);
                            answer_node.addEventListener('click', function(e) {
                                let currentActive = q_node.querySelector('.active');
                                if (currentActive) currentActive.classList.remove('active');
                                this.classList.toggle('active');
                            });
                            q_node.querySelector('ul').appendChild(answer_node);
                        })
                    });

                innerQuiz.appendChild(q_node);
            });

            let submitQuiz = createNode(`
                <button class="green_normal_button submit_quiz" id="submit_quiz">Submit</button>
            `);
            submitQuizEvent(submitQuiz, room, quiz, user);
            innerQuiz.appendChild(submitQuiz);

            let answered = db.collection('rooms')
                .doc(room.id)
                .collection('quizzes')
                .doc(quiz.id)
                .collection('answered');

            answered.onSnapshot(snap => {
                // See totals only if voted
                answered.doc(user.uid)
                    .get()
                    .then(doc => {
                        if (doc.exists) {
                            let submit = document.getElementById('submit_quiz');
                            if (submit) submit.remove();
                            calculateTotals(room.id, quiz.id);
                            setCorrectAnswers(room.id, quiz.id);
                        }
                    })
                    .catch(e => console.log(e.message));
            });
        });
}


function submitQuizEvent(submitNode, room, quiz, user) {
    submitNode.addEventListener('click', function (e) {
        // Get answers size
        let quiz_doc = db.collection('rooms')
            .doc(room.id)
            .collection('quizzes')
            .doc(quiz.id);

        quiz_doc.collection('questions')
            .get()
            .then(snap => {

                let answered = quiz_doc.collection('answered');
                answered
                    .doc(user.uid)
                    .get()
                    .then(doc => {
                        if (doc.exists) {
                            addMessage('You already voted for this quiz!');
                        }
                        else {
                            let nrQuestions = snap.size;
                            // Get all active answers
                            let activeAnswers = document.querySelectorAll('.quiz_answer.active');
                            if (nrQuestions !== activeAnswers.length) {
                                addMessage('Please answer all the questions!', 'error');
                            }
                            registerAnswers(room, quiz, user, quiz_doc, snap);
                        }
                    })
                    .catch(e => console.log(e.message));

            })
            .catch(e => console.log(e.message));
    });
}

function registerAnswers(room, quiz, user) {
    getQuizQuestions(room.id, quiz.id)
        .then(questions => {
            questions.forEach(question => {
                let questionNode = document.querySelector(`ul[data-quiz-question='${question.id}'`);
                if (questionNode) {
                    let answerNode = questionNode.querySelector('.quiz_answer.active');
                    let user_answer_id = answerNode.dataset.quizAnswer;

                    getAnswerCount(room.id, quiz.id, question.id, user_answer_id)
                        .then(count => {
                            db.collection('rooms')
                                .doc(room.id)
                                .collection('quizzes')
                                .doc(quiz.id)
                                .collection('questions')
                                .doc(question.id)
                                .collection('answers')
                                .doc(user_answer_id)
                                .collection('counter')
                                .doc('answered')
                                .set({'counter' : count + 1 });
                        })
                        .catch(e => console.log(e.message));
                }
            });
        });
    registerAnswered(room.id, quiz.id, user.uid);
}

function registerAnswered(room_id, quiz_id, user_uid) {
    let doc_ref = db.collection('rooms')
        .doc(room_id)
        .collection('quizzes')
        .doc(quiz_id)
        .collection('answered')
        .doc(user_uid);

    doc_ref
        .get()
        .then(doc => {
            // Register the vote
            if (!doc.exists) {
                doc_ref
                    .set({'timestamp': + new Date()})
                    .then(r => {
                        addMessage('Your vote has been registered!', 'success');
                    })
                    .catch(e => console.log(e.message));
            }

        })
        .catch(e => console.log(e.message));
}


function calculateTotals(room_id, quiz_id) {
    getQuizQuestions(room_id, quiz_id)
        .then(questions => {
            questions.forEach(question => {
                let questionNode = document.querySelector(`ul[data-quiz-question='${question.id}'`);
                if (questionNode) {
                    let promises = [];
                    getQuestionAnswers(room_id, quiz_id, question.id)
                        .then(answers => {
                            answers.forEach(answer => {
                                let promise = getAnswerCount(room_id, quiz_id, question.id, answer.id);
                                promises.push(promise);
                            });
                            return answers;
                        })
                        .then(answers => {
                            Promise.all(promises)
                                .then(arr => {
                                    let total = arr.reduce((acc, v) => acc+v, 0);
                                    for (let index = 0; index < answers.length; index++) {
                                        let answer = answers[index];
                                        let answerNode = document.querySelector(`[data-quiz-answer='${answer.id}'`);
                                        let cnt = arr[index];
                                        let proportion = 100 * cnt / total;
                                        let n = createNode(`
                                                <span>
                                                    ${answer.data().content}<span class="ans_percent">
                                                        ${proportion.toFixed(2)}%
                                                    </span>
                                                </span>
                                            `);
                                        answerNode.innerHTML = '';
                                        answerNode.appendChild(n);
                                    }
                                })
                                .catch(e => console.log(e.message));
                        })
                        .catch(e => console.log(e.message));
                }
            })
        });
}

function setCorrectAnswers(room_id, quiz_id) {
    getQuizQuestions(room_id, quiz_id)
        .then(questions => {
            questions.forEach(question => {
                let questionNode = document.querySelector(`ul[data-quiz-question='${question.id}'`);
                if (questionNode) {
                    getQuestionAnswers(room_id, quiz_id, question.id)
                        .then(answers => {
                            answers.forEach(answer => {
                                db.collection('rooms')
                                    .doc(room_id)
                                    .collection('quizzes')
                                    .doc(quiz_id)
                                    .collection('questions')
                                    .doc(question.id)
                                    .collection('answers')
                                    .doc(answer.id)
                                    .collection('private')
                                    .get()
                                    .then(snap => {
                                        let corr = snap.docs[0].data();
                                        let questionNode = document.querySelector(`ul[data-quiz-question='${question.id}'`);
                                        let answerNode = document.querySelector(`[data-quiz-answer='${answer.id}'`);
                                        if (corr.correct) {
                                            answerNode.classList.add('correct');
                                        }
                                        else {
                                            answerNode.classList.add('incorrect');
                                        }
                                    })
                                    .catch(e => console.log(e.message));
                            })

                        });
                }
            });
        });
}



export function getQuizQuestions(room_id, quiz_id) {
    return new Promise((res, _) => {
        let col = db.collection('rooms')
            .doc(room_id)
            .collection('quizzes')
            .doc(quiz_id)
            .collection('questions');

        let questions = [];
        col.get()
            .then(snap => {
                snap.forEach(question => {
                    questions.push(question);
                });
                res(questions);
            })
            .catch(e => {
                console.log(e.message);
            });

    })

}

export function getQuestionAnswers(room_id, quiz_id, question_id) {
    return new Promise((res, rej) => {
        let col = db.collection('rooms')
            .doc(room_id)
            .collection('quizzes')
            .doc(quiz_id)
            .collection('questions')
            .doc(question_id)
            .collection('answers');

        col
            .get()
            .then(snap => {
                res(snap.docs);
            })
            .catch(e => {
                console.log(e.message);
                rej();
            });

    })
}

export function getAnswerCount(room_id, quiz_id, question_id, answer_id) {
    return new Promise((res, rej) => {
        let doc_ref = db.collection('rooms')
            .doc(room_id)
            .collection('quizzes')
            .doc(quiz_id)
            .collection('questions')
            .doc(question_id)
            .collection('answers')
            .doc(answer_id)
            .collection('counter')
            .doc('answered');

        doc_ref
            .get()
            .then(doc => {
                res(doc.exists ? doc.data().counter : 0);
            })
            .catch(_ => rej());
    })
}


export function setupQuizzes(room, user) {

    db.collection('rooms')
        .doc(room.id)
        .collection('quizzes')
        .where('status', '==', 'active')
        .get()
        .then(snap => {
            if (snap.size > 0) {
                let quiz = snap.docs[0].data();
                quiz.id = snap.docs[0].id;
                createQuiz(room, user, quiz);

                db.collection('rooms')
                    .doc(room.id)
                    .collection('quizzes')
                    .doc(quiz.id)
                    .onSnapshot(doc => {
                        if (doc.data().status === 'ended') {
                            addMessage('Quiz has ended', 'success');
                            setTimeout(_ => location.replace('/room'), 1500);
                        }
                    })
            }
        })
        .catch(e => console.log(e.message));

}