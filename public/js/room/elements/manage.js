import {getQuizQuestions} from './quizzes.js';
import {getAnswerCount, getQuestionAnswers} from "./quizzes.js";

export function setupManagement(room, user) {
    if (room.uid === user.uid) {
        let mainContainer = document.getElementById('manage');
        let quizManageContainer = createNode(`
            <div class="quiz_manage_container">
                <h2>Manage Quizzes</h2>
            </div>
        `);
        db.collection('rooms')
            .doc(room.id)
            .collection('quizzes')
            .get()
            .then(snap => {
                snap.forEach(doc => {
                    addQuizManangeComponent(room, doc, user, quizManageContainer);
                });
                return snap;
            })
            .then(snap => {
                if (snap.size > 0) {
                    mainContainer.appendChild(quizManageContainer);
                }
            })
            .then(_ => {
                setupStatistics(room, user);
            });

    }
}

function setupPanel() {}

function setupStatistics(room, user) {
    let mainContainer = document.getElementById('manage');
    let statisticsContainer = createNode(`
        <div class="statistics">
            <h2>Statistics</h2>
            <div class="statistics_row">
                <div class="statistics_panel">
                    <div class="header">
                        <h4>Number of users</h4>
                    </div>
                    <div class="body">
                        <p class="number_of_users"></p>
                    </div>
                </div>
                <div class="statistics_panel">
                    <div class="header">
                        <h4>Number of messages</h4>
                    </div>
                    <div class="body">
                        <p class="number_of_messages"></p>
                    </div>
                </div>
                <div class="statistics_panel">
                    <div class="header">
                        <h4>Number of messages votes</h4>
                    </div>
                    <div class="body">
                        <p class="number_of_votes"></p>
                    </div>
                </div>
            </div>
        </div>
    `);


    db.collection('rooms')
        .doc(room.id)
        .collection('joined')
        .onSnapshot(snap => {
            statisticsContainer.querySelector('.number_of_users').innerHTML = snap.size;
        });

    let messRef = db.collection('rooms')
        .doc(room.id)
        .collection('messages');

    messRef
        .onSnapshot(snap => {
            statisticsContainer.querySelector('.number_of_messages').innerHTML = snap.size;
            let sum = 0;
            let promisses = [];
            snap.forEach(message => {
                let promise = messRef.doc(message.id).collection('voted')
                    .get()
                    .then(voted => {
                        return voted.size;
                    });
                promisses.push(promise);
            });
            Promise.all(promisses).then(result => {
                statisticsContainer.querySelector('.number_of_votes').innerHTML = result.reduce((acc, v) => acc+v, 0);
            })
        });

    db.collection('rooms')
        .doc(room.id)
        .collection('quizzes')
        .get()
        .then(snap => {
            snap.forEach(quiz => {
                let quizNode = createNode(`
                    <div class="quiz_statistics">
                        <div class="quiz_info">
                            <div class="quiz_title">
                                <h3>${quiz.data().name}</h3>
                            </div>
    
                           
                            
                        </div>
                    </div>
                `);

                getQuizQuestions(room.id, quiz.id)
                    .then(questions => {
                        questions.forEach(question => {
                            let questionNode = createNode(`
                                <div class="question_statistics_wrapper">
                                    <div class="question_title">
                                        <h3>${question.data().content}</h3>
                                    </div>
        
                                    <div class="question_statistic">
                                    <ul></ul>
                                    </div>
                                </div>
                            `);

                            let promises = [];
                            getQuestionAnswers(room.id, quiz.id, question.id)
                                .then(answers => {
                                    answers.forEach(answer => {
                                        let promise = getAnswerCount(room.id, quiz.id, question.id, answer.id);
                                        promises.push(promise);
                                    });
                                    return answers;
                                })
                                .then(answers => {
                                    Promise.all(promises)
                                        .then(arr => {
                                            let total = arr.reduce((acc, v) => acc+v, 0);
                                            for (let index = 0; index < answers.length; index++) {
                                                let answer = answers[index];
                                                let cnt = arr[index];
                                                let proportion = 100 * cnt / total;
                                                let str = (total === 0) ? 'no responses' : `${cnt}/${total}`;
                                                let answerNode = createNode(`
                                                    <li>
                                                        <span>
                                                        ${answer.data().content}
                                                        </span> 
                                                        <span class="ans_percent">${str}</span>
                                                        ${ total !== 0 ? `<span class="ans_percent">
                                                            ${proportion.toFixed(2)}%
                                                        </span>` : ''}
                                                    </li>
                                                `);
                                                questionNode.querySelector('ul').appendChild(answerNode);
                                            }
                                        })
                                        .catch(e => console.log(e.message));
                                })
                                .then(_ => {
                                    quizNode.appendChild(questionNode);
                                })
                                .catch(e => console.log(e.message));
                        })
                    })
                    .then(_ => {
                        statisticsContainer.appendChild(quizNode);
                    });
            })
        })
        .then(_ => {
            mainContainer.appendChild(statisticsContainer);
        });

}

function manageQuiz(room, quiz, user) {
    let quizRef = db
        .collection('rooms')
        .doc(room.id)
        .collection('quizzes');



    quizRef.doc(quiz.id).get().then(doc => {
        if (doc.data().status === 'active') {
            quizRef.doc(quiz.id).update({'status': 'ended'});
        }
        else if (doc.data().status === 'inactive') {
            quizRef
                .get()
                .then(snap => {
                    snap.forEach(elem => {
                        if (elem.data().status === 'active') {
                            quizRef.doc(elem.id).update({'status' : 'ended'});
                        }
                    })
                })
                .then(_ => {
                    quizRef.doc(quiz.id).update({'status': 'active', 'started_at': +new Date()});
                })

        }
    });
}


function addQuizManangeComponent(room, quiz, user, parent) {
    let answered = db.collection('rooms')
        .doc(room.id)
        .collection('quizzes')
        .doc(quiz.id)
        .collection('answered');

    answered.get().then(snap => {
            let text = {
                'active' : 'Stop',
                'inactive': 'Start',
                'ended' : 'Ended'
            };
            let buttonText = text[quiz.data().status];
            let manageNode = createNode(`
                <div class="quiz_manage" data-id="${quiz.id}">
                    <span class="pill_label">${quiz.data().name}</span>
                    <span class="pill_label participants">Participants: ${snap.size}</span>
                    <span class="pill_label status ${quiz.data().status}"></span>
                    <span class="pill_label pill_button button_${quiz.data().status}"></span>
                </div>
            `);
            parent.appendChild(manageNode);

        manageNode.querySelector('.pill_button').addEventListener('click', function(e) {
                manageQuiz(room, quiz, user);
            });

            answered.onSnapshot(changes => {
                changes.docChanges().forEach(change => {
                    if (change.type === 'added') {
                        manageNode.querySelector('.participants').innerHTML = `Participants: ${changes.size}`;
                    }
                });
            });

            db.collection('rooms')
                .doc(room.id)
                .collection('quizzes')
                .doc(quiz.id)
                .onSnapshot(doc => {
                    let statusSpan = manageNode.querySelector('.status');
                    statusSpan.classList.remove(statusSpan.classList[statusSpan.classList.length - 1]);
                    statusSpan.classList.add(doc.data().status);
                    statusSpan.innerHTML = `Status: ${doc.data().status}`;

                    let buttonSpan = manageNode.querySelector('.pill_button');
                    buttonSpan.classList.remove(buttonSpan.classList[buttonSpan.classList.length - 1]);
                    buttonSpan.classList.add(`button_${doc.data().status}`);
                    manageNode.querySelector('.pill_button').innerText = text[doc.data().status];
                })

        })


}