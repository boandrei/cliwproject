export function addClarityFeedback() {
    let node = createNode(`
        <div class="clarity_wrapper">
            <h3>Clarity</h3>
            <div class="like_buttons" id="like_buttons">
                <button class="green_normal_button">I understand</button>
                <button class="green_normal_button">I do not understand</button>
            </div>
        </div>
    `);

    let buttons = node.querySelectorAll('button');
    for (let i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener('click', function () {

            buttons.forEach(el => {
                el.classList.remove('active')
            });

            buttons[i].classList.add('active')
        })
    }
    document.getElementById('feedback_types').appendChild(node);
}

export function addFeedbackMessage() {
    let node = createNode(`
        <form class="feedback_message_component" action="" id="search_form" method="POST">

            <div class="find_question_wrapper">
                <label for="inp" class="inp">
                    <input type="text" id="search_input" placeholder="&nbsp;">
                    <span class="label">Search for a question</span>
                    <span class="border"></span>
                </label>
            </div>

            <select class="message_limit drop" name="feedback_messages_limit" id="feedback_messages_limit">
                <option class="option" value="5">5</option>
                <option class="option" value="10">10</option>
                <option class="option" value="20">20</option>
                <option class="option" value="30">30</option>
            </select>
        </form>
    `);
    document.getElementById('feedback_types').appendChild(node);
}

export function addGradeFeedback() {
    let node = createNode(`
        <div class="grade_wrapper" id="grades">
            <h3>Grade</h3>
            <div class="grades">
                <div id="grade_1" class="grade" grade="1">1</div>
                <div id="grade_2" class="grade" grade="2">2</div>
                <div id="grade_3" class="grade" grade="3">3</div>
                <div id="grade_4" class="grade" grade="4">4</div>
                <div id="grade_5" class="grade" grade="5">5</div>
                <div id="grade_6" class="grade" grade="6">6</div>
                <div id="grade_7" class="grade" grade="7">7</div>
                <div id="grade_8" class="grade" grade="8">8</div>
                <div id="grade_9" class="grade" grade="9">9</div>
                <div id="grade_10" class="grade" grade="10">10</div>
            </div>
        </div>`
    );
    let grades = node.querySelectorAll('.grade')
    document.getElementById('feedback_types').appendChild(node)


    for (let i = 0; i < grades.length; i++) {
        grades[i].addEventListener('click', function () {
            let data = {'grade': grades[i].getAttribute('grade')};

            makeRequest('POST', '/modify-grade', JSON.stringify(data)).then(response => {


            }).catch(error => {

            });

            grades.forEach(el => {
                el.classList.remove('active')
            });

            grades[i].classList.add('active')
        })
    }

}
