import {setupOutline} from "./elements/outline.js";
import {setupButtons} from './elements/buttons.js';
import {setupNames} from './elements/names.js';
import {setupQuizzes} from './elements/quizzes.js';
import {setupFeedback} from './elements/messages.js'
import {setupManagement} from './elements/manage.js'

function setupRoom(room, user) {
    setupNames(room, user);
    setupButtons(room, user);
    setupOutline(room, user);
    setupFeedback(room, user);
    setupQuizzes(room, user);
    setupManagement(room, user);
    /*
    if (response.data.feedback.grade) {
        feedbackModule.addGradeFeedback();

        let gradeElement = document.getElementById('grade_' + response.data.grade_feedback);

        if (gradeElement) {
            gradeElement.classList.add('active')
        }
    }

    if (response.data.feedback.clarity) {
        feedbackModule.addClarityFeedback();
    }

    if (response.data.feedback.form_message) {
        feedbackModule.addFeedbackMessage();
        let searchInput = document.getElementById("search_input"),
            feedbackMessageLimit = document.getElementById('feedback_messages_limit'),
            feedbackMessageComponents = document.querySelectorAll('.feedback_message_component');

        feedbackMessageComponents.forEach(el => {
            el.style = 'display: block'
        });

        feedbackMessagesSearch(searchInput.value);

        let myEfficientFn = debounce(function () {
            feedbackMessagesSearch(searchInput.value);
        }, 250);

        searchInput.addEventListener('input', myEfficientFn);
        feedbackMessageLimit.addEventListener('change', myEfficientFn);
    }
    */
}


document.addEventListener('DOMContentLoaded', function() {
    let room_id = getCookie('room_id');
    if (!room_id) {
        addMessage('Such room doesn\'t exist', 'error');
    }
    else {
        firebase.auth().onAuthStateChanged(user => {
            db.collection('rooms')
                .doc(room_id)
                .get()
                .then(doc => {
                    let room = doc.data();
                    room.id = doc.id;
                    setupRoom(room, user);
                })
                .catch(e => {
                    addMessage(e.message, 'error');
                    //setTimeout(_ => location.replace('/'), 1500);
                })
        })
    }

});



