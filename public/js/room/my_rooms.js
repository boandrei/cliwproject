function addRoom(room, room_id) {
    let roomNode = createNode(`
        <li data-room-id="${room_id}">       
            <div class="node_seg">
                <span class="pill_label">
                    Name: ${room.room_name}
                </span>
            </div>      
            <div class="node_seg"> 
                <span class="pill_label">
                   Topic: ${room.room_topic}
                </span>  
            </div> 
            <div class="node_seg">             
                <span class="pill_label">
                   Key: ${room.access_key}
                </span>
            </div>
            
            <div class="node_seg half_seg">
                <a data-room-key="${room.access_key}" data-room-id="${room_id}" class="link_as_button goto">
                    Go To
                </a>
            </div>
            
            <div class="node_seg half_seg">
                <a data-room-id="${room_id}" class="link_as_button delete">
                    Delete    
                </a>
            </div>
        </li>
    `);

    roomNode.querySelector('.goto').onclick = e => {
        let roomKey = e.target.dataset.roomKey;
        let roomId = e.target.dataset.roomId;
        setCookie('access_key', roomKey);
        setCookie('room_id', roomId);
        location.replace(`/room`);
    };

    roomNode.querySelector('.delete').onclick = e => {
        let roomId = e.target.dataset.roomId;
        firebase.auth().onAuthStateChanged(user => {
            db.collection('rooms').doc(roomId).delete()
                .then(_ => {
                    addMessage('Room has been deleted', 'success');
                })
                .catch(e => {
                    addMessage('Having trouble deleting, try again later', 'error');
                    console.log(e.message);
                })
        })
    };

    document.querySelector('ul#created_rooms').appendChild(roomNode);
}

function removeRoom(room_id) {
    document.querySelector(`li[data-room-id="${room_id}"`).remove();
}

document.addEventListener('DOMContentLoaded', function() {
    firebase.auth().onAuthStateChanged(user => {
        db.collection('rooms')
            .where('uid', '==', user.uid)
            .onSnapshot(snapshot => {
                snapshot.docChanges().forEach(change => {
                    if (change.type === 'added') {
                        addRoom(change.doc.data(), change.doc.id);
                    }
                    else if (change.type === 'removed') {
                        removeRoom(change.doc.id);
                    }
                });

                if (snapshot.size === 0) {
                    addMessage('There are no rooms to show, please create one', 'error');
                }

                return snapshot;
            });
    })

});