var UtilLib = {
    'createNode' : function (code) {
        let template = document.createElement('template');
        template.innerHTML = code.trim();
        return template.content.firstChild;
    },
    'firstParentWithClass' : function (element, className) {
        let current = element;
        do {
            current = current.parentElement;
        } while (
            current
            && current.classList
            && !current.classList.contains(className)
            );
        return current;
    }

};

var QuizLib = {
    'markAnswer' : function (e) {
        let box = this.parentElement;
        if (box.classList.contains('correct')) {
            box.classList.remove('correct');
            box.querySelector('label[for^=answer_').innerHTML = 'wrong';
        }
        else {
            box.classList.add('correct');
            box.querySelector('label[for^=answer_').innerHTML = 'correct';
        }
        QuizLib.validateData();
    },

    'addAnswer' : function () {
        let answerNode = QuizLib.createAnswerNode(
            {
                "correct" : false,
                "content" : ""
            }
        );
        let container = UtilLib.firstParentWithClass(this, 'quiz_answer');
        container.parentElement.insertBefore(answerNode, container.nextSibling);
        QuizLib.renameQuizData();
        QuizLib.validateData();
    },

    'addQuestion' : function () {
        let questionNode = QuizLib.createQuestionNode(
            {
                "content" : "",
                "answers" : [
                    {
                        "correct" : true,
                        "content" : ""
                    }
                ]
            }
        );

        let container = UtilLib.firstParentWithClass(this, 'question_create');
        container.parentElement.insertBefore(questionNode, container.nextSibling);
        QuizLib.renameQuizData();
        QuizLib.validateData();
    },

    'addQuiz' : function () {
        let quizNode = QuizLib.createQuizNode(
            {
                "duration" : 5,
                "name" : "",
                "questions" : [
                    {
                        "content" : "",
                        "answers": [
                            {
                                "correct" : true,
                                "content" : ""
                            }
                        ]

                    }
                ]
            }
        );
        let container = UtilLib.firstParentWithClass(this, 'quiz_create');
        container.parentElement.insertBefore(quizNode, container.nextSibling);
        QuizLib.renameQuizData();
        QuizLib.validateData();
    },

    'renameQuizData' : function () {
        let quizzes = document.querySelectorAll('.quiz_create:not(.bogus)');
        for (let [index, quiz] of quizzes.entries()) {
            let name = `quiz_name_${index}`;
            let label  = quiz.querySelector('label[for^=quiz_');
            let input = quiz.querySelector('input[name^=quiz_');
            let duration = quiz.querySelector('.time_create');
            let duration_label = duration.querySelector('label[for^=quiz_duration_');
            let duration_input = duration.querySelector('input[name^=quiz_duration_');
            let duration_name = `quiz_duration_${index}`;

            label.htmlFor = name;
            label.innerHTML = `Quiz ${index + 1}`;

            input.id = name;
            input.name = name;

            duration_label.htmlFor = duration_name;
            duration_input.name = duration_name;
            duration_input.id = duration_name;

            let questions = quiz.querySelectorAll('.question_create');
            for (let [q_index, question] of questions.entries()) {
                let name = `question_content_${index}_${q_index}`;
                let label = question.querySelector('label[for^=question_content_');
                let input = question.querySelector('input[name^=question_content_]');
                label.htmlFor = name;
                label.innerHTML = `Question ${q_index + 1}`;

                input.id = name;
                input.name = name;

                let answers = question.querySelectorAll('.quiz_answer');
                for (let [a_index, answer] of answers.entries()) {
                    let name = `answer_content_${index}_${q_index}_${a_index}`
                    let label = answer.querySelector('label[for^=answer_content_');
                    let input = answer.querySelector('input[name^=answer_content_]');
                    label.htmlFor = name;

                    input.name = name;
                    input.id = name;
                }
            }
        }
    },

    'createAnswerNode' : function (answer) {
        let answerNode = UtilLib.createNode(
            `<li class="quiz_answer ${ (answer.correct === true) ? 'correct' : ''  }">
                <div class="is_correct check_toggle ${ (answer.correct === false) ? 'off' : ''}">
                    <div></div>
                </div>
                <div class="input_box">
                    <label for="answer_content_">${ (answer.correct === true) ? 'correct' : 'wrong' }</label>
                    <input type="text" name="answer_content_" placeholder="Type answer" 
                    id="answer_content_" value="${ answer.content }">
                    <span>
                        <span class="add_answer_button plus">+</span>
                        <span class="remove_answer delete">×</span>
                    </span>
                </div>
            </li>`
        );
        answerNode.querySelector('.check_toggle').addEventListener('click', function(e) {
            this.classList.toggle('off');
            QuizLib.validateData();
        });
        answerNode.querySelector('.check_toggle').addEventListener('click', QuizLib.markAnswer);

        answerNode.querySelector('.plus').addEventListener('click', QuizLib.addAnswer);

        answerNode.querySelector('.remove_answer').addEventListener('click', function(e) {
           let answer = UtilLib.firstParentWithClass(this, 'quiz_answer');
           let question = UtilLib.firstParentWithClass(this, 'question_create');
           if (question.querySelectorAll('.quiz_answer').length > 1)
           {
               answer.parentElement.removeChild(answer);
               QuizLib.renameQuizData();
               QuizLib.validateData();
           }

        });
        for (let input of answerNode.querySelectorAll('input')) {
            input.oninput = QuizLib.validateData;
        }
        return answerNode;
    },

    'createQuestionNode' : function (question) {
        let questionNode = UtilLib.createNode(
            `<div class="question_create">
                <div>
                    <div class="input_box">
                        <span>
                            <span class="plus add_question_button">+</span>
                            <span class="remove_question delete">×</span>
                        </span>
                        <label for="question_content_">Question</label>
                        <input type="text" name="question_content_" placeholder="Type question" id="question_content_" 
                          value="${question.content}">
                    </div>
                </div>
                <ul>
                </ul>
            </div>`
        );

        for (let input of questionNode.querySelectorAll('input')) {
            input.oninput = QuizLib.validateData;
        }

        questionNode.querySelector('.remove_question').addEventListener('click', function(e) {
            let question = UtilLib.firstParentWithClass(this, 'question_create');
            let quiz = UtilLib.firstParentWithClass(this, 'quiz_create');
            if (quiz.querySelectorAll('.question_create').length > 1) {
                question.parentElement.removeChild(question);
                QuizLib.renameQuizData();
                QuizLib.validateData();
            }
        });

        questionNode.querySelector('.add_question_button').addEventListener('click', QuizLib.addQuestion);
        for (let answer of question.answers) {
            let answerNode = QuizLib.createAnswerNode(answer);
            questionNode.querySelector('ul').appendChild(answerNode);
        }

        return questionNode;
    },

    'createQuizNode' : function (quiz) {
        let quizNode = UtilLib.createNode(
        `<div class="quiz_create">
            <div class="quiz_details">
                <div class="input_box">
                    <span>
                        <span class="plus add_quiz_button">+</span>
                        <span class="delete remove_quiz">×</span>
                    </span>
                    
                    <label for="quiz_">Quiz</label>
                    <input type="text" name="quiz_" placeholder="Type quiz name" id="quiz_"
                      value="${ quiz.name }">
                </div>

                <div class="time_create">
                    <label for="quiz_duration_">Duration (minutes): </label>
                    <input type="number" min="1" max="1000" id="quiz_duration_" 
                      name="quiz_duration_" value="${ quiz.duration }">
                </div>
            </div>
        </div>`
        );

        for (let input of quizNode.querySelectorAll('input')) {
            input.oninput = QuizLib.validateData;
        }

        quizNode.querySelector('.remove_quiz').addEventListener('click', function(e) {
            let quiz = UtilLib.firstParentWithClass(this, 'quiz_create');
            quiz.parentElement.removeChild(quiz);
            QuizLib.renameQuizData();
            QuizLib.validateData();
        });

        quizNode.querySelector('.add_quiz_button').addEventListener('click', QuizLib.addQuiz);
        for (let question of quiz.questions) {
            let questionNode = QuizLib.createQuestionNode(question);
            quizNode.appendChild(questionNode);
        }

        return quizNode;
    },

    'importQuizzes' : function (data) {
        if (data.quizzes) {
            for (let quiz of data.quizzes) {
                let quizNode = QuizLib.createQuizNode(quiz);
                document.querySelector('.quizzes').appendChild(quizNode);
            }
            QuizLib.renameQuizData();
        }
        else {
            console.error('JSON has no quizzes property');
        }
    },


    'exportQuizzes' : function() {
        data = { 'quizzes' : [] };
        let quizzes = document.querySelectorAll('.quiz_create:not(.bogus)');
        for (let [q_index, quiz] of quizzes.entries()) {
            let quiz_data = {};
            quiz_data.duration = quiz.querySelector('input[name^=quiz_duration').value;
            quiz_data.name = quiz.querySelector('input[name^=quiz_name').value;
            if (!quiz_data.name) {
                quiz_data.name = `Quiz ${q_index + 1}`;
            }
            quiz_data.questions = [];

            let questions = quiz.querySelectorAll('.question_create');
            for (let [qu_index, question] of questions.entries()) {
                let question_data = {};
                question_data.content = question.querySelector('input[name^=question_content').value;
                if (!question_data.content) {
                    return new Error(`Question ${ qu_index + 1} doesn't have content`);
                }
                question_data.answers = [];

                let answers = question.querySelectorAll('.quiz_answer');
                let hasCorrect = false;
                for (let [a_index, answer] of answers.entries()) {
                    let answer_data = {};
                    answer_data.correct = answer.classList.contains('correct');
                    if (answer_data.correct) {
                        hasCorrect = true;
                    }
                    answer_data.content = answer.querySelector('input[name^=answer_content').value;
                    if (!answer_data.content) {
                        return new Error(`Answer ${a_index + 1}, from Question ${ qu_index + 1 } doesn't have content`);
                    }
                    question_data.answers.push(answer_data);
                }
                if (!hasCorrect) {
                    return new Error(`Question ${qu_index + 1} has no correct answer`);
                }
                quiz_data.questions.push(question_data);
            }
            data.quizzes.push(quiz_data);
        }
        return data;
    },

    'validateData' : function() {
        result = QuizLib.exportQuizzes();
        if (result instanceof Error) {
            document.querySelector('.quiz_error').innerHTML = result.toString();
        }
        else {
            document.querySelector('.quiz_error').innerHTML = '';
        }
    }
};

let OutlineLib = {

    'fixAll' : function (node, index_str) {
        if (node.classList.contains('outline_chapter')) {
            node.querySelector('.chapter_number').innerHTML = index_str;
            let input = node.querySelector('input[name^=chapter_');
            input.name = `chapter_${index_str}`;
            input.id = `chapter_${index_str}`;
        }
        let item_index = 1;
        let list = node.querySelector('ul');
        if (list) {
            for (let item of list.children) {
                OutlineLib.fixAll(item, `${index_str}.${item_index}`);
                item_index++;
            }
        }
    },

    'fixNumbering' : function() {
        for (let [index, item] of document.querySelectorAll('.outline > ul > .outline_chapter').entries()) {
            OutlineLib.fixAll(item, String(index+1));
        }
    },


    'createChapter' : function (data) {
        let chapter = UtilLib.createNode(
            `<li class="outline_chapter">
                 <div class="input_box">
                    <span class="chapter_number">1.</span>
                    <span class="plus add_chapter">⭳</span>
                    <span class="under add_chapter_under">⮑</span>
                    <span class="delete remove_chapter">×</span>
                    <input type="text" name="chapter_" placeholder="Enter Title" id="chapter_"
                           value="${ data.content }">
                 </div>
                 <ul></ul>
            </li>`
        );

        chapter.querySelector('.add_chapter').addEventListener('click', OutlineLib.addChapter);
        chapter.querySelector('.add_chapter_under').addEventListener('click', OutlineLib.addChapterUnder);
        chapter.querySelector('.remove_chapter').addEventListener('click', OutlineLib.removeChapter);
        return chapter;
    },

    'removeChapter': function (e) {
        let chapter = UtilLib.firstParentWithClass(this, 'outline_chapter');
        if (!chapter.classList.contains('first_chapter')) {
            chapter.remove();
        }
        OutlineLib.fixNumbering();
    },

    'addChapter' : function (e) {
        let chapter = UtilLib.firstParentWithClass(this, 'outline_chapter');
        let newChapter = OutlineLib.createChapter({"content" : ""});
        chapter.parentElement.insertBefore(newChapter, chapter.nextSibling);

        OutlineLib.fixNumbering();
    },

    'addChapterUnder' : function (e) {
        let chapter = UtilLib.firstParentWithClass(this, 'outline_chapter');
        let newChapter = OutlineLib.createChapter({"content" : ""});
        let underList = chapter.querySelector('ul');
        underList.appendChild(newChapter);
        OutlineLib.fixNumbering();
    },

    'exportAll' : function (node, chapters) {
        let list = node.querySelector('ul');
        let value = node.querySelector('input[name^=chapter_]').value;
        if (!value) {
            value = node.querySelector('input[name^=chapter_]').name.replace('_', ' ');
            value = value.charAt(0).toUpperCase() + value.slice(1);
        }
        if (list) {
            let obj = { "content" : value, "subchapters" : []};
            for (let item of list.children) {
                OutlineLib.exportAll(item, obj.subchapters);
            }
            chapters.push(obj);
        }
        else {
            chapters.push({ "content" : value } );
        }
    },

    'exportChapters' : function () {
        let chapters = {};
        chapters.chapters = [];
        for (let item of document.querySelectorAll('.outline > ul > .outline_chapter')) {
            OutlineLib.exportAll(item, chapters.chapters);
        }
        return chapters;
    },

    'importAll' : function(node, chapters) {
        //console.log(node);
        for (let chapter of chapters) {
            let newNode = OutlineLib.createChapter(chapter);
            if (chapter.subchapters) {
                OutlineLib.importAll(newNode, chapter.subchapters);
            }
            node.querySelector('ul').appendChild(newNode);
        }
    },

    'importChapters' : function(data) {
        let mainList = document.querySelector('.outline');
        mainList.querySelector('ul').innerHTML = '';
        OutlineLib.importAll(mainList, data);
        OutlineLib.fixNumbering();
    }
};

function exportData() {
    let room_data = {};
    room_data.room_name = document.getElementById('room_name').value;
    room_data.room_topic = document.getElementById('room_topic').value;
    room_data.room_votes = parseInt(document.getElementById('room_votes').value);
    room_data.allow_anonymous = !document.getElementById('allow_anonymous').classList.contains('off');
    room_data.feedback_type = [];
    if (!document.getElementById('feedback_form').classList.contains('off')) {
        room_data.feedback_type.push('form_message');
    }
    if (!document.getElementById('grade').classList.contains('off')) {
        room_data.feedback_type.push('grade');
    }
    let result = QuizLib.exportQuizzes();
    if (result instanceof Error) {
        return result;
    }
    room_data.current_chapter = '1';
    room_data.outline = OutlineLib.exportChapters().chapters;
    let quizzes = result.quizzes;
    return {
        'room_data': room_data,
        'quizzes': quizzes,
    };
}

function sendAnswer(answer, answer_index, room_id, quiz_id, question_id) {
    let answer_data = {
        'content': answer.content,
        'index': answer_index
    };

    let answers = db.collection('rooms')
        .doc(room_id)
        .collection('quizzes')
        .doc(quiz_id)
        .collection('questions')
        .doc(question_id)
        .collection('answers');

    answers.add(answer_data)
        .then(added_answer => {
            // Make answer hidden behind security rules
            answers
                .doc(added_answer.id)
                .collection('private')
                .add({
                    'correct' : answer.correct
                });
        })
}

function sendQuestion(question, question_index, room_id, quiz_id) {
    let question_data = {
        'content': question.content,
        'index': question_index
    };
    db.collection('rooms')
        .doc(room_id)
        .collection('quizzes')
        .doc(quiz_id)
        .collection('questions')
        .add(question_data)
        .then(added_question => {
            for (let [answer_index, answer] of question.answers.entries()) {
                sendAnswer(answer, answer_index, room_id, quiz_id, added_question.id);
            }
        })
}

function sendQuiz(quiz, quiz_index, room_id) {
    let quiz_data = {
        'status': 'inactive',
        'duration': quiz.duration,
        'name': quiz.name,
        'index': quiz_index
    };

    return db.collection('rooms')
        .doc(room_id)
        .collection('quizzes')
        .add(quiz_data)
        .then(added_quiz => {
            for (let [question_index, question] of quiz.questions.entries()) {
                sendQuestion(question, question_index, room_id, added_quiz.id);
            }
        })

}

async function sendData(e) {

    firebase.auth().onAuthStateChanged(async user => {
        let exp = exportData();
        let roomKey = makeid(5);
        let finishSearching = false;
        while (!finishSearching) {
            let res = await db.collection('room_keys').doc(roomKey)
                .get()
                .then(doc => {
                    if (doc.exists) roomKey = makeid(5);
                    else finishSearching = true;
                });
        }

        if (exp instanceof Error) {
            addMessage(data.message, "error");
            return;
        }
        exp.room_data.uid = user.uid;
        exp.room_data.access_key = roomKey;
        let batch = db.batch();
        let roomsCollectionRef = db.collection('rooms').doc();
        let keysRef = db.collection('room_keys').doc(roomKey);

        batch.set(roomsCollectionRef, exp.room_data);
        batch.set(keysRef, {'room_id' : roomsCollectionRef.id});
        batch.commit()
            .then(r => {
                let quizData = [];
                for (let [quiz_index, quiz] of exp.quizzes.entries()) {
                    quizData.push(sendQuiz(quiz, quiz_index, roomsCollectionRef.id));
                }
                return Promise.all(quizData);
            })
            .then(_ => {
                addMessage('Room successfully created!', 'success');
            })
            .catch(e => {
                addMessage(e.message, 'error');
            })
    });


    return false;
}


document.addEventListener('DOMContentLoaded', function (_) {
    for (let answer_plus of document.querySelectorAll('.add_answer_button')) {
        answer_plus.addEventListener('click', QuizLib.addAnswer);
    }
    for (let question_plus of document.querySelectorAll('.add_question_button')) {
        question_plus.addEventListener('click', QuizLib.addQuestion);
    }
    for (let quiz_plus of document.querySelectorAll('.add_quiz_button')) {
        quiz_plus.addEventListener('click', QuizLib.addQuiz);
    }
    for (let chapter_plus of document.querySelectorAll('.add_chapter')) {
        chapter_plus.addEventListener('click', OutlineLib.addChapter);
    }
    for (let chapter_under of document.querySelectorAll('.add_chapter_under')) {
        chapter_under.addEventListener('click', OutlineLib.addChapterUnder);
    }
    for (let chapter_remove of document.querySelectorAll('.remove_chapter')) {
        chapter_remove.addEventListener('click', OutlineLib.removeChapter);
    }
    QuizLib.renameQuizData();
    OutlineLib.fixNumbering();

});