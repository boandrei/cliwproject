function enterRoom() {
    let access_key = document.getElementById('room_passcode').value;
    if (!access_key) {
        addMessage('You need to enter the passcode first', 'error');
        return false;
    }

    firebase.auth().onAuthStateChanged(user => {
        db.collection('room_keys').doc(access_key)
            .get()
            .then(doc => {
                if (!doc.exists) {
                    addMessage('Such room doesn\'t exist', 'error');
                }
                else {
                    setCookie('access_key', access_key);
                    setCookie('room_id', doc.data().room_id);

                    db.collection('rooms').doc(doc.data().room_id)
                        .collection('joined')
                        .add({'id' : user.uid})
                        .then(_ => {
                            location.replace('/room');
                        })
                }
            })
            .catch(e => {
                console.log(`Firebase error: ${e.code}: ${e.message}`);
                addMessage(e.message, 'error');
            });
    })


    return false;
}

(function(){
    document.getElementById('enter_room_form').addEventListener('submit', function(event) {
        event.preventDefault();
        return enterRoom();
    });
    document.getElementById('enter_room').addEventListener('click', e => {
        enterRoom();
    })
})();
