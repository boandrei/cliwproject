function debounce(func, wait, immediate) {
    let timeout;
    return function () {
        let context = this, args = arguments;
        let later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        let callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}

function firstParentWithClass(element, className) {
    let current = element;
    do {
        current = current.parentElement;
    } while (
        current
        && current.classList
        && !current.classList.contains(className)
        );
    return current;
}

function scrollFunction(element, elementTop) {
    if (window.pageYOffset > elementTop) {
        element.classList.add("top_sticky");
    } else {
        element.classList.remove("top_sticky");
    }
}

function get_time_diff(date) {
    let datetime = new Date(date).getTime(),
        now = new Date().getTime();

    if (isNaN(datetime)) {
        return "";
    }

    let distance = datetime - now;

    let days = Math.floor(distance / (1000 * 60 * 60 * 24)),
        hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)),
        minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)),
        seconds = Math.floor((distance % (1000 * 60)) / 1000);

    if (distance < 0) {
        clearInterval();
        return 'EXPIRED';
    }

    seconds = seconds > 0 ? seconds + "s " : (minutes > 0 ? '0s ' : '');
    minutes = minutes > 0 ? minutes + "m " : (hours > 0 ? '0m ' : '');
    hours = hours > 0 ? hours + "h " : (days > 0 ? '0h ' : '');
    days = days > 0 ? days + "d " : '';

    return days + hours + minutes + seconds;
}

function hasClass(el, className) {
    if (el.classList)
        return el.classList.contains(className);
    return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
}

function addClass(el, className) {
    if (el.classList)
        el.classList.add(className);
    else if (!hasClass(el, className))
        el.className += " " + className;
}

function removeClass(el, className) {
    if (el.classList)
        el.classList.remove(className);
    else if (hasClass(el, className)) {
        var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
        el.className = el.className.replace(reg, ' ');
    }
}

function createNode(code) {
    let template = document.createElement('template');
    template.innerHTML = code.trim();
    return template.content.firstChild;
}

function makeid(n) {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (let i = 0; i < n; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function changeSlide(newSlide) {
    let current = document.querySelector('.current_slide');
    current.classList.remove('current_slide');
    let newContainer = document.querySelector(`.${newSlide}.slide`);
    newContainer.classList.add('current_slide');
}

function to(promise) {
    return promise.then(data => {
        return [null, data];
    })
    .catch(err => [err]);
}