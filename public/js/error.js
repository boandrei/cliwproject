function addMessage(message, status = 'success') {
    let template = document.createElement('template');
    template.innerHTML = `
    <div class="alert ${status}">${message}</div>
    `.trim();

    let node = template.content.firstChild;
    let alerts = document.getElementById('alerts');
    if (alerts.children.length > 3) {
        alerts.removeChild(alerts.children[0]);
    }
    alerts.appendChild(node);
    node.addEventListener('click', function (e) {
        this.parentNode.removeChild(this);
    });

    setTimeout(() => {
        if (alerts.children.length > 0) {
            alerts.removeChild(alerts.children[0]);
        }
    }, 5000);
}
