function makeRequest(method, url, data={}, auth=false) {
    return new Promise(function (resolve, reject) {
        let headers = {
            "Content-Type": "application../public/json"
        };

        let request = {
            method: method,
            headers: headers
        };

        let xhr = new XMLHttpRequest();
        xhr.open(method, url);
        xhr.setRequestHeader("X-CSRF-TOKEN", document.head.querySelector("[name=csrf-token]").content);
        if (auth) {
            let cookie = getCookie('api_key');
            if (cookie) {
                xhr.setRequestHeader('Authorization', cookie);
            }
        }

        xhr.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                resolve(JSON.parse(xhr.response));
            } else {
                reject({
                    status: this.status,
                    statusText: xhr.statusText,
                    response: JSON.parse(xhr.response),
                });
            }
        };
        xhr.onerror = function () {
            reject({
                status: this.status,
                statusText: xhr.statusText,
                response: JSON.parse(xhr.response),
            });
        };
        xhr.send(data);
    });
}

/**
 * Returns a promise that the response json body will parse
 * Also, the promise result is a new json, that contains the response status
 * and the effective json body send by the server
 * @param response
 * @returns {Promise<any>}
 */
function parseJSON(response) {
    return new Promise((resolve, reject) => response.json()
        .then(json => resolve({
            status: response.status,
            ok: response.ok,
            json,
        }))
        .catch(error => reject(new Error('An unexpected error has happened'))));
}

/**
 * Sends an asynchronous request to a given api url and returns a promise
 * If the response status is 200, resolves the promise with the response json body
 * If the response status is not 200, rejects the promise with the response json body
 *
 * @param {string} method Can be GET, POST, PUT, DELETE or other HTTP methods
 * @param {string} url API endpoint to which the request is made
 * @param {object} data Optional parameter that contains the request body
 *               it is ignored with GET requests
 * @param {boolean} auth Whether the authentication is required and the api_key
 *               should be attached to the request
 */
function api(method, url, data={}, auth=false) {
    return new Promise((resolve, reject) => {
        let headers = {
            "Content-Type": "application../public/json"
        };
        if (auth) {
            let cookie = getCookie('api_key');
            if (cookie) {
                headers['Authorization'] = cookie;
            }
        }
        let request = {
            method: method,
            headers: headers
        };

        if (method.toLowerCase() !== 'get') {
            request.body = JSON.stringify(data);
        }
        fetch(url, request)
        .then(parseJSON)
        .then(response => {
            if (!('message' in response.json)) {
                response.json.message = (response.ok ? 'Success!' : 'Failure');
            }
            response.json.status = response.status;
            if (response.ok) {
                return resolve(response.json);
            }

            return reject(response.json);
        })
        .catch(error => {
            reject({ 'message' : error.message});
        });
    });
}