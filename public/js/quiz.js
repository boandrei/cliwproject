(function () {


    let quizSubmitButton = document.getElementById('submit_quiz');
    let access_key = getCookie('access_key');

    quizSubmitButton.addEventListener('click', function (e) {
        e.preventDefault();

        let quiz_id = document.getElementById('inner_quiz').dataset['quizId'];
        document.getElementById('quiz').classList.add('submitted');
        let answers = {'answers': []};
        document.querySelectorAll('.quiz_answer.active').forEach(value => {
            let answer = {};
            let chosenAnswer = value.dataset['quizAnswer'];
            let question = value.parentElement;
            answer.question_id = question.dataset['quizQuestion'];
            answer.answer_id = chosenAnswer;
            answers['answers'].push(answer);
        });

        api('POST', `/api/room/${access_key}/quiz/${quiz_id}/submit`, answers, true)
            .then(response => {
                addMessage('Quiz answers have been registered', 'success');
                if (response.data.show_result) {
                    let numberOfCorrectAnswers = 0;
                    document.querySelectorAll('.quiz_answer').forEach(value => {
                        let answerId = parseInt(value.dataset['quizAnswer']);
                        let question = value.parentElement;
                        let questionId = parseInt(question.dataset['quizQuestion']);
                        let currentQuestion = response.data.questions.filter(q =>
                            q.question === questionId
                        )[0];
                        let currentAnswer = currentQuestion['answers'].filter(a =>
                            a.id === answerId
                        )[0];

                        let proportion = 100 * currentAnswer.chosen_count/response.data.total_answers;



                        let correct = currentQuestion['answers'].filter(a =>
                            a.is_correct
                        ).map(a => a.id);

                        if (correct.indexOf(answerId) !== -1) {
                            value.classList.add('correct');
                            numberOfCorrectAnswers++;
                        }
                        else {
                            value.classList.add('incorrect');
                        }
                        if (value.classList.contains('active')) {
                            value.innerHTML += '<span class="your_answer">your answer</span>'
                        }

                    });
                    // document.getElementById('quiz_result')
                    //     .innerText('<p>' + numberOfCorrectAnswers + ' / ' + correctAnswers.length +  '</p>')

                }
            })
            .catch(response => {
                addMessage(response.message, 'error');
            });

    })

})();