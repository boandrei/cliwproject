

function registerUser() {
    let username = document.getElementById('username').value;
    let email = document.getElementById('email').value;
    let password = document.getElementById('password').value;
    let repeat_password = document.getElementById('repeat').value;

    firebase.auth().onAuthStateChanged(user => {
        if (password !== repeat_password) {
            addMessage('Passwords don\'t match', 'error');
            return;
        }
        if (user && !user.isAnonymous) {
            addMessage('You are already registered', 'error');
            setTimeout(_ => location.replace('/'), 1500);
        }
        else if (user && user.isAnonymous) {
            let credential = firebase.auth.EmailAuthProvider.credential(email, password);
            firebase.auth().currentUser.linkAndRetrieveDataWithCredential(credential)
                .then(_ => {
                    addMessage('Register successful', 'success');
                    setTimeout(_ => location.replace('/'), 1500);
                }).catch(e => {
                    console.log(`Firebase error: ${e.code}: ${e.message}`);
                    addMessage(e.message, 'error');
                });
        }
        else {
            firebase.auth().createUserWithEmailAndPassword(email, password)
                .then(_ => {
                    addMessage('Register successful', 'success');
                    setTimeout(_ => location.replace('/'), 1500);
                })
                .catch(e => {
                    console.log(`Firebase error: ${e.code}: ${e.message}`);
                    addMessage(e.message, 'error');
                });
        }
    });
    return false;
}

function sendLoginData() {
    let email = document.getElementById('email').value;
    let password = document.getElementById('password').value;

    // TODO add maybe some additional validations?
    firebase.auth().signInWithEmailAndPassword(email, password)
        .then(_ => {
            addMessage('Logged in!', 'success');
            setTimeout(_ => location.replace('/'), 1500);
        })
        .catch(e => {
            console.log(`Firebase error: ${e.code}: ${e.message}`);
            addMessage(e.message, 'error');
        });

    return false;
}

function logout() {
    firebase.auth().signOut()
        .then(_ => {
        addMessage('Logout successful', 'success');
        setTimeout(_ => location.replace('/'), 1500);
    }).catch(e => {
        console.log(`Firebase error: ${e.code}: ${e.message}`);
        addMessage(e.message, 'error');
    });
    //setTimeout(_ => location.reload(), 1500);
}

function updateAuthButtons(user) {
    let list = document.getElementById('main_navbar').querySelector('ul');

    if (user && !user.isAnonymous) {
        let register = document.getElementById('menu_register');
        if (register) register.remove();
        let login = document.getElementById('menu_login');
        if (login) login.remove();

        let myRoomsButton = createNode(`
            <li id="menu_rooms">
                <a href="/rooms">My Rooms</a>
            </li>`
        );
        let logoutButton = createNode(`
            <li id="menu_logout">
                <a href="javascript:void(0)">Logout</a>
            </li>`
        );
        logoutButton.addEventListener('click', logout);
        list.insertBefore(myRoomsButton, list.childNodes[0]);
        list.appendChild(logoutButton);
    }
    else {
        let rooms = document.getElementById('menu_rooms');
        if (rooms) rooms.remove();
        let logout = document.getElementById('menu_logout');
        if (logout) logout.remove();

        let loginButton = createNode(`
            <li id="menu_login">
                <a href="/login">Login</a>
            </li>`
        );
        let registerButton = createNode(`
            <li id="menu_register">
                <a href="/register">Register</a>
            </li>`
        );
        list.appendChild(registerButton);
        list.appendChild(loginButton);
    }

}

document.addEventListener('DOMContentLoaded', function (_) {
    firebase.auth().onAuthStateChanged(function(user) {
        if (!user) {
            firebase.auth().signInAnonymously()
                .catch(e => {
                    console.log(`Firebase error: ${e.code}: ${e.message}`);
                    addMessage(e.message, 'error');
                });
        }
        else {
            updateAuthButtons(user);
        }
    });
});