
document.addEventListener('DOMContentLoaded', function (_) {
    window.onresize = removeNav;

    function removeNav() {
        let width = document.body.clientWidth;

        if (width > 640) {
            navBar.classList.remove("opened");
            this.innerHTML = "Touch for more";
        }
    }

    let navBarTriggerOpen = document.getElementById("main_navbar_trigger_open");
    let navBar = document.getElementById("main_navbar");

    navBarTriggerOpen.addEventListener("click", function (e) {
        if (navBar.classList.contains("opened"))
        {
            navBar.classList.remove("opened");
            this.innerHTML = "Touch for more";
        }
        else
        {
            navBar.classList.add("opened");
            this.innerHTML = "Touch for less";
        }

    }, false);

});
