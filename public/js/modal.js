(function () {
    let modal = document.getElementById('myModal'),
        btn = document.getElementById("modal_trigger"),
        closeButton = document.querySelectorAll(".modal_close");

    if (btn) {
        btn.addEventListener("click", function (e) {
            modal.style.display = "block";
        });
    }


    for (let i = 0; i < closeButton.length; i++) {
        closeButton[i].addEventListener("click", function () {
            modal.style.display = "none";
        });
    }

    window.addEventListener("click", function (event) {
        if (event.target === modal) {
            modal.style.display = "none";
        }
    });
})();